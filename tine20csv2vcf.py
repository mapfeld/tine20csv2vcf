#!/usr/bin/python3

#
# by Marian Patrik Felder <code@marian-felder.de>
# 
# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.#
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import csv
import sys

def ga(array, indicies):
    ret = ''
    for i in indicies:
        ret += array[i]
        if (i != indicies[-1]):
            ret += ';'
    return ret

def ne(array, indicies):
    ret = False
    if (isinstance(indicies,list)):
        ret = True
        for i in indicies:
            ret = ret and array[i] != ''
    else:
        return array[indicies] != ''
    return ret

def convert(somefile):
    #assuming tine_20 export format
    with open( somefile, 'r' ) as source:
        reader = csv.reader( source )
        vcf = ''
        i = 0
        with open("all.vcf", 'w') as f:
            #https://www.evenx.com/vcard-3-0-format-specification
            for row in reader:
                i += 1
                if (i < 2):
                    continue
                #write in individual vcf
                vcf = ""
                vcf += ( 'BEGIN:VCARD' + "\n")
                vcf += ( 'VERSION:3.0' + "\n")
                # N*:LASTNAME; FIRSTNAME; ADDITIONAL NAME; NAME PREFIX(Mr.,Mrs.); NAME SUFFIX
                salu = 'Mr.' if row[41] == 'MR' else 'Mrs.' if row[41] == 'MS' else ''
                vcf += ( 'N:' + ga(row,[26, 29, 30]) + ";" + salu + ";" + row[32] + "\n")
                # # FN:FORMATTED_NAME
                # vcf += ( 'FN:' + row[1] + ' ' + row[0] + "\n")
                # BDAY: YYYY-MM-DD
                if (ne(row,17)):
                    vcf += ( 'BDAY:' + row[17].split(' ')[0] + "\n")
                if (ne(row,34)):
                    vcf += ( 'ORG:' + row[34] + "\n")
                if (ne(row,65)):
                    vcf += ( 'TITLE:' + row[65] + "\n")
                # Post Office Address; Extended Address; Street; Locality; Region; Postal Code; Country
                vcf += ( 'ADR;TYPE=WORK:;' + ga(row,[5,4,1,3,2,0]).replace('\n','\\n') + "\n")
                vcf += ( 'ADR;TYPE=HOME:;' + ga(row,[13,12,9,11,10,8]).replace('\n','\\n') + "\n")
                if (ne(row,45)):
                    vcf += ( 'TEL;TYPE=WORK,CELL:' + row[45] + "\n")
                if (ne(row,51)):
                    vcf += ( 'TEL;TYPE=WORK,VOICE:' + row[51] + "\n")
                if (ne(row,47)):
                    vcf += ( 'TEL;TYPE=WORK,FAX:' + row[47] + "\n")
                if (ne(row,46)):
                    vcf += ( 'TEL;TYPE=HOME,CELL:' + row[46] + "\n")
                if (ne(row,49)):
                    vcf += ( 'TEL;TYPE=HOME,VOICE:' + row[49] + "\n")
                if (ne(row,48)):
                    vcf += ( 'TEL;TYPE=HOME,FAX:' + row[48] + "\n")
                if (ne(row,50)):
                    vcf += ( 'TEL;TYPE=HOME,PAGER:' + row[50] + "\n")
                if (ne(row,19)):
                    vcf += ( 'EMAIL;type=internet:' + row[19] + "\n")
                if (ne(row,20)):
                    vcf += ( 'EMAIL;type=internet:' + row[20] + "\n")
                if (ne(row,68)):
                    vcf += ( 'URL:' + row[68] + "\n")
                if (ne(row,25)):
                    vcf += ( 'NOTE:' + row[25].replace('\n','\\n') + "\n")
                vcf += ( 'END:VCARD' + "\n")
                vcf += ( "\n")

                # i += 1#counts
                # break
                f.write(vcf)
        print(vcf)

        print (str(i) + " contacts converted")


def main(args):
    if len(args) != 2:
        print ( "Usage:")
        print ( args[0] + " filename")
        exit(1)

    convert(args[1])

if __name__ == '__main__':
    main(sys.argv)
    exit(0)
