# tine20csv2vcf

## Info

This script is meant to convert a csv file exported by Tine2.0 groupware to vCard version 3.0 format.

## Usage

tine20csv2vcf.py <input csv file>

Writes a '''all.vcf''' file to the current directory.

## Known issues

Currently creates private and public addesses, even if they are empty
